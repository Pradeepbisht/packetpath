/**********************************************************
 * Name			: fileCompare.c
 *
 * Description	: Program to Compare two Binary Files and 
 * 				  Print the Byte where they differ.
 *
 * Created By 	: Pradeep Singh Bisht
 *********************************************************/
#include <stdio.h>
#include <fileCompare.h> 
void cmpBinaryFiles(FILE *,FILE *);

int main(int argc, char *argv[])
{
	FILE *fp1, *fp2;
	// Checking the Command Line arguments enter properly
	if (argc != MAX_CMD_LIMIT)
	{
		printf("\nError: Number of Arguments given are not correct:");
		printf("\nUsage:./fileCompare <filename1> <filename2>");
		return FAIL;
	}
	else
	{
		//opening files and error handling
		fp1 = fopen(argv[1],"r");
		if (fp1 == NULL)
		{
			printf("\nError in opening file %s", argv[1]);
			return FAIL;
		}
		fp2 = fopen(argv[2], "r");
		if (fp2 == NULL)
		{
			printf("\nError in opening file %s", argv[1]);
			return FAIL;
		}
		//Sending to the function to compare
		cmpBinaryFiles(fp1, fp2);
	}
	return SUCCESS;
}

/**********************************************************
 * Name			: fileCompare.c
 *
 * Description	: Compare two Binary Files and Print the 
 * 				  Byte next 16 bytes where they differ.
 * Return Value : NONE 
 * Created By 	: Pradeep Singh Bisht
 *********************************************************/
void cmpBinaryFiles(FILE *fp1, FILE *fp2)
{
	char bytePtr1, bytePtr2;
	int byteCount = 0;
	bytePtr1 = fgetc(fp1);    //bytePtr for first file
	bytePtr2 = fgetc(fp2);    //bytePte for second file
	
	//Loop to check for the files identical or not
	while ((bytePtr1 != EOF) && (bytePtr2 != EOF))
	{
		if (bytePtr1 == bytePtr2)
		{
			bytePtr1 = fgetc(fp1);
			bytePtr2 = fgetc(fp2);
			continue;
		}
		else
		{
			break;
		}
	}
	//Print if identitical 
	if ((bytePtr1 == EOF) && (bytePtr2 == EOF))
	{
		printf("\nFiles are Identical");
	}
	else
	{
		//Calculate the offset at each file and print the next 16 bytes MAX if possible
		if (bytePtr1 != EOF)	
		{
			printf("\nFiles are not Identical, First Byte of file1 they differ at offset %ld\n",ftell(fp1));
			while (bytePtr1 != EOF && byteCount < MAX_BYTE_COUNT)
			{
				printf("%c",bytePtr1);	
				bytePtr1 = fgetc(fp1);
				byteCount ++;
			}
		}
		byteCount = 0;

		if (bytePtr2 != EOF)	
		{
			printf("\nFiles are not Identical, First Byte of file2 they differ at offset %ld \n",ftell(fp2));
			while (bytePtr2 != EOF && byteCount < MAX_BYTE_COUNT)
			{
				printf("%c",bytePtr2);	
				bytePtr2 = fgetc(fp2);
				byteCount ++;
			}
		}
	}
}

