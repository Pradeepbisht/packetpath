/**********************************************************
 * Name         : fileCompare.h
 *
 * Description  : Contains the Macros and declaration of the 
 * 				  functions.
 * 
 * Created By   : Pradeep Singh Bisht
 **********************************************************/

//Macro Definition
#define MAX_CMD_LIMIT 3	 			//Limit to check the number of command line arguments
#define MAX_BYTE_COUNT 16			//Limit to the Byte to be printed
#define FAIL 0
#define SUCCESS 1

//Function Declarations	
void compareBinaryFiles(FILE *,FILE *);			// Function declaration of the comare function
